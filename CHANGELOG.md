# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.2] - 2025-03-01

### Added
- Added Bloodmoon shrubs (Thanks @RonikCZ) to default grass IDs (!10)
- Added some IDs from Tamriel Rebuilt's Narsis release and Project Cyrodiil's Anvil release (Thanks @RonikCZ) to default grass IDs (!9)

### Fixed
- Fixed duplicated meshes if there were multiple references to the same mesh with different cases

## [0.2.1] - 2024-06-08

### Fixed
- Activators are no longer turned into groundcover by default. This causes issues and it only affects a handful of objects (#1, #3).
- Groundcover is no longer generated for interior cells (fixes #5)
- Added a scripted grass static from Quests for Clans and Vampire Legends (QCVL) to the blacklist (thanks to @RonikCZ in !7)
- Fixed issue where non-static objects were being matched and generated as groundcover (#12)

## [0.2.0] - 2023-12-09
- Added more groundcover IDs (thanks to @hristoast in !3)
- Added more excluded IDs (thanks to @RonikCZ in !4)
- Added an optional `--delta-plugin-exe` argument for specifying the path to a `delta_plugin` executable (thanks to @hristoast in !5).

## [0.1.1] - 2023-09-01
- Exclude `cliffgrass` ids by default (from OAAB), as they usually refer to cliffs with grass on them
  (!1 and !2, submitted by @hristoast).

## [0.1.0] - 2023-07-22
### Added
- Initial implementation which generates groundcover from static identifiers, using delta_plugin, 
  configurable using a toml file 
