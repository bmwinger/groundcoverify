#!/usr/bin/env python3

# This file is part of Groundcoverify!.
#
# Copyright (C) 2023 Benjamin Winger
#
#  Groundcoverify! is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  Groundcoverify! is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with Groundcoverify!.  If not, see <https://www.gnu.org/licenses/>.

try:
    import tomllib
except ImportError:
    import tomli as tomllib

import logging
import os
import re
import shutil
import subprocess
import sys
from argparse import ArgumentParser
from logging import debug, info

if __name__ == "__main__":
    parser = ArgumentParser(
        description="A script to turn objects in regular OpenMW plugins into groundcover"
    )
    parser.add_argument(
        "-v", "--verbose", help="Output additional information", action="store_true"
    )
    parser.add_argument(
        "-c",
        "--config",
        help="groundcoverify config file path",
        nargs="?",
        default=os.path.join(os.path.dirname(__file__), "groundcoverify.toml"),
    )
    parser.add_argument("--openmw-config", help="OpenMW config file path")
    parser.add_argument(
        "--pretend",
        help="Only display the matched identifiers and mesh files. Implies verbose",
        action="store_true",
    )
    parser.add_argument(
        "--ignore",
        help="Ignore plugins with the given name. By default only the output filename is ignored.",
        nargs="*",
    )
    parser.add_argument(
        "--delta-plugin-exe",
        help="Optional path to a delta_plugin executable.",
    )

    args = parser.parse_args(sys.argv[1:])
    if args.verbose or args.pretend:
        logging.root.setLevel(logging.DEBUG)
    # Read config from toml
    with open(args.config, "rb") as file:
        config = tomllib.load(file)

    # Defaults are trivial and will not produce meaningful output
    output_directory = config.get("output_directory", ".")
    groundcover_output_filename = config.get(
        "groundcover_output", "groundcover.omwaddon"
    )
    deleted_output_filename = config.get(
        "deleted_output", "deleted_groundcover.omwaddon"
    )

    output_groundcover = os.path.join(output_directory, groundcover_output_filename)
    output_deleted = os.path.join(output_directory, deleted_output_filename)

    ids_expr = "|".join(config.get("grass_ids", []))
    if config.get("exclude"):
        ids_expr = (
            "^(?!.*(" + "|".join(config["exclude"]) + ").*).*(" + ids_expr + ").*$"
        )
    debug(f"Using id regex {ids_expr}")
    delta_plugin = shutil.which("delta_plugin")
    if args.delta_plugin_exe:
        delta_plugin = args.delta_plugin_exe
    if delta_plugin is None:
        raise Exception("Could not find delta_plugin! Make sure it is in your PATH")
    dp_version = subprocess.check_output(
        [delta_plugin, "--version"], encoding="utf-8"
    ).strip()
    _, _, dp_version = dp_version.partition(" ")
    if tuple(map(int, dp_version.split("."))) < (0, 19, 0):
        raise Exception(
            f"You are using delta_plugin version {dp_version}, but at least version 0.19 is required!"
        )

    openmw_config = args.openmw_config or config.get("openmw_config")

    delta_plugin_args = [delta_plugin]
    if openmw_config:
        delta_plugin_args += ["-c", openmw_config]
    if args.verbose:
        delta_plugin_args.append("-vv")

    ignored_plugin_args = []
    ignored_names = (args.ignore or []) + config.get("ignored_plugins", [])
    for plugin in ignored_names:
        ignored_plugin_args.extend(["--ignore", plugin])

    info("The following statics and activators have been turned into groundcover")
    mesh_paths = set()
    groundcover_ids = set()

    query_args = delta_plugin_args + ["query", "--all"]
    object_types = set(config.get("object_types", []))
    for object_type in object_types:
        query_args.extend(["match", object_type, "--id", ids_expr])

    # TODO: Use this output to find the exact IDs of the objects which should be turned into groundcover
    # The cellref match command will match any reference matching the ID, regardless of object type
    # So we need to be more specific with the objects matched
    for line in subprocess.check_output(query_args, encoding="utf-8").splitlines():
        if any(line.startswith(f'"{object_type}::') for object_type in object_types):
            match = re.match('^"[A-Za-z]+::(.*)":$', line)
            if not match:
                raise RuntimeError(f"Could not parse output {line}")
            groundcover_ids.add(match.group(1))
        elif line.strip().startswith("model:"):
            # Find model in VFS matching the path, and symlink/hardlink/copy it to grass/path
            _, _, path = line.strip().partition(" ")
            pathlist = path.strip('"').split("\\")
            if pathlist[0] != "grass":
                path = os.path.join(*pathlist).lower()
                if path not in mesh_paths:
                    mesh_paths.add(path)

    ids_expr = "|".join(groundcover_ids)

    # Only match against exterior cells since interior cells don't support groundcover
    exterior_cell_regex = r"^[0-9\-]+x[0-9\-]+$"
    if args.pretend:
        filter_args = (
            delta_plugin_args
            + [
                "query",
                "--all",
                "--ids",
                "--ignore",
                deleted_output_filename,
            ]
            + ignored_plugin_args
        )
        for object_type in config["object_types"]:
            # Change paths of matching objects
            filter_args += ["match", object_type, "--id", ids_expr]
        subprocess.check_call(filter_args)
    else:
        # fmt: off
        filter_args = delta_plugin_args + [
            "filter", "--all", "--output", output_groundcover,
            "--desc", "Generated groundcover plugin from vanilla-style groundcover\n"
            "Should be accompanied by a regular content plugin deleting the groundcover\n",
            # Copy cell references
            "match", "Cell", "--cellref-object-id", ids_expr, "--id", exterior_cell_regex
        ]
        for object_type in config["object_types"]:
            # Change paths of matching objects
            filter_args += [
                "match", object_type, "--id", ids_expr,
                "--modify", "model", "^(?!grass\\\\)", "grass\\"
            ]
        # fmt: on
        subprocess.check_call(filter_args)

    if not args.pretend:
        # fmt: off
        subprocess.check_call(delta_plugin_args + [
            "filter", "--all", "--output", output_deleted
        ] + ignored_plugin_args + [
            "--desc", "Generated Deleted Groundcover\n"
            "to be accompanied by a generated groundcover plugin\n",
            # Delete cell references
            "match", "Cell", "--cellref-object-id", ids_expr, "--id", exterior_cell_regex, "--delete"
        ])
        # fmt: on

    for path in sorted(mesh_paths):
        if not args.pretend:
            path = path.lower()
            new_path = os.path.join(output_directory, "meshes", "grass", path)

            os.makedirs(os.path.dirname(new_path), exist_ok=True)
            # Extract mesh from VFS
            subprocess.check_output(
                delta_plugin_args
                + ["vfs-extract", os.path.join("Meshes", path), new_path],
                encoding="utf-8",
            ).strip()
