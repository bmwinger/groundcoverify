# Groundcoverify!

This is a simple python script which uses [DeltaPlugin](https://gitlab.com/bmwinger/delta-plugin) to turn regular groundcover in morrowind plugins into openmw-style groundcover.

## Usage

Launch by running the `groundcoverify.py` script, which will produce a `groundcover.omwaddon`, `deleted-groundcover.omwaddon` and a `Meshes` directory within the current directory. These should be placed into the OpenMW VFS.

`./groundcoverify.py`

The `groundcover.omwaddon` file should be added to `openmw.cfg` in the `groundcover` section, and `deleted-groundcover.omwaddon` should be added in the `content` section.

Default settings, including the output directory and names of the plugins, can be configured by editing `groundcoverify.toml`.

### Usage with merged plugins

DeltaPlugin's merged plugin and Groundcoverify's deleted_groundcover plugin will both end up with each other in their masters lists, so one needs to be ignored by the other to avoid a circular dependency after the first run (unless you modify `openmw.cfg` to remove one of them prior to generation).

Since merge tools in general might re-introduce or remove groundcover, it is probably better to have merge tools ignore the deleted groundcover plugin and run groundcoverify after the merge tool to make sure that all the generated groundcover is consistent with the merged plugin (it's unlikely that it will make a difference, as it probably will only occur with multiple mods making changes to the same landscape which likely aren't very compatible anyway). E.g. run `delta_plugin merge --ignore deleted_groundcover.omwaddon`.

## Requirements

Either python 3.11 or newer, or Python 3.7 or newer with tomli installed, and [DeltaPlugin 0.19 or newer](https://gitlab.com/bmwinger/delta-plugin).

Note that there is a bug with deleting cellrefs in OpenMW 0.47 which prevents this from working properly. It has been fixed in OpenMW 0.48.

## Issues
Issues can be reported to either [the GitLab issue tracker](https://gitlab.com/bmwinger/groundcoverify/-/issues) or [the GitLab Email Service Desk](mailto:contact-project+bmwinger-groundcoverify-46758398-issue-@incoming.gitlab.com).